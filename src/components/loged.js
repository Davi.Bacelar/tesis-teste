import React, { useState } from 'react'
import Axios from 'axios'
import { useSelector, useDispatch } from 'react-redux';
import { authAction } from '../actions'
import { Redirect } from 'react-router'
const Icons = {}

const LoginComp = (props) => {
    const dispatch = useDispatch();
    const auth = useSelector(state => state.auth)

    const logout = (e) => {
        dispatch(authAction(false))
    }

    return (
        <div>
            {auth == true && 
                <>
                    <h1>Logado</h1>
                    <button onClick={_ => logout()}>Deslogar</button>
                </>
            }

            {auth != true && 
                <Redirect to="/login" />
            }
        </div>
    )
}

export default LoginComp